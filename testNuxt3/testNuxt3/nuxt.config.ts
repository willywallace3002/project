import { defineNuxtConfig } from 'nuxt';

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
	buildModules: ['@pinia/nuxt'],
	modules: ['@nuxtjs/strapi'],
	strapi: {
		url: 'http://localhost:1337'
	}
});
