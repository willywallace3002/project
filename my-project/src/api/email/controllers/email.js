'use strict';

const { sendstuff } = require("../services/email");
const { createCoreController } = require('@strapi/strapi').factories;
/**
 * A set of functions called "actions" for `email`
 */

 module.exports =  createCoreController('api::email.email', ({ strapi }) =>  ({
  
  async sendstuff(ctx) {
    console.log(ctx.request.body)
    const data = ctx.request.body.data
    if(!data){
      ctx.send({message : "no data"}, 500)
      return
    }
    console.log(data)
    const { adresse } = data // const adresse = data.adresse
    if (!adresse){
      ctx.send({message : "email requis"}, 500)
      return
    }
    const emailRegex = new RegExp( 
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
    if (!emailRegex.test(adresse)) {
      ctx.send({message : "email invalide"}, 500)
      return
    } 
    console.log(adresse)
    console.log("mail valide")
    try { 
    await strapi.plugins['email-designer'].services.email.sendTemplatedEmail(
      {
        to: adresse,
        
      },
      {
        templateReferenceId: 1,
        
      },
      {
        USER: {
          email: adresse
        }
      }
    );
    ctx.send({ message: "mail envoyé",success: true}, 200)
  } catch (err) {
    strapi.log.debug('📺: ', err);
    ctx.send({ message: "Une erreur s'est produite !" }, 500);
  }
    
  },
}));