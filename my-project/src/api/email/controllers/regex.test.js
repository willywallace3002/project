
test('Teste si la regex fonctionne', () => {
    const exemple = 'zefzefze'
    const emailRegex = new RegExp( 
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
    expect(exemple).toMatch(emailRegex)
})