"use strict";
const { createCoreService } = require("@strapi/strapi").factories;
module.exports = createCoreService(
"api::email.email",
({ strapi }) => ({
async sendstuff () {

    await strapi.plugins['email'].services.email.send({
        to: 'valid email address',
        from: 'Excited User <me@samples.mailgun.org>', //e.g. single sender verification in SendGrid
        cc: '',
        bcc: '',
        replyTo: '',
        subject: 'The Strapi Email plugin worked successfully',
        text: 'Hello world!',
        html: 'Hello world!',
      })
      



}}
))
