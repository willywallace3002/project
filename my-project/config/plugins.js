module.exports = ({ env }) => ({
    // ...
    email: {
      config: {
        provider: 'mailgun',
        providerOptions: {
          key: env('mailkey'), // Required
          domain: env('maildomain'), // Required
          url: env('MAILGUN_URL', 'https://api.mailgun.net'), //Optional. If domain region is Europe use 'https://api.eu.mailgun.net'
        },
        settings: {
          defaultFrom: 'Excited User <me@samples.mailgun.org>',
          defaultReplyTo: 'willywallace3002@gmail.com',
        },
      },
    },
    // ...


    // ...
  'email-designer': {
    enabled: true,

    // ⬇︎ Add the config property
    config: {
      editor: {

        tools: {
          heading: {
            properties: {
              text: {
                value: 'This is the new default text!',
              },
            },
          },
        },
        options: {
          features: {
            colorPicker: {
              presets: ['#D9E3F0', '#F47373', '#697689', '#37D67A'],
            },
          },
          fonts: {
            showDefaultFonts: false,
            /*
             * If you want use a custom font you need a premium unlayer account and pass a projectId number :-(
             */
            customFonts: [
              {
                label: 'Anton',
                value: "'Anton', sans-serif",
                url: 'https://fonts.googleapis.com/css?family=Anton',
              },
              {
                label: 'Lato',
                value: "'Lato', Tahoma, Verdana, sans-serif",
                url: 'https://fonts.googleapis.com/css?family=Lato',
              },
              // ...
            ],
          },
          mergeTags: [
            {
              name: 'Email',
              value: '{{= USER.username }}',
              sample: 'john@doe.com',
            },
            // ...
          ],
        },
        appearance: {
          theme: 'dark',
          panels: {
            tools: {
              dock: 'left',
            },
          },
        },
      },
    },
  },
  // ...

});